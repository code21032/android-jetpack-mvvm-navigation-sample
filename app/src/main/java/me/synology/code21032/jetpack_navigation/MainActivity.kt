package me.synology.code21032.jetpack_navigation

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import me.synology.code21032.jetpack_navigation.ui.main.MainFragment

class MainActivity : AppCompatActivity() {

    lateinit var navHost: NavHostFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        //set navigation host
        navHost = supportFragmentManager
                .findFragmentById(R.id.navHost) as NavHostFragment
        NavigationUI.setupActionBarWithNavController(this, navHost.navController)
    }

    //back button
    override fun onSupportNavigateUp(): Boolean {
        return navHost.navController.navigateUp() || super.onSupportNavigateUp()
    }

}
